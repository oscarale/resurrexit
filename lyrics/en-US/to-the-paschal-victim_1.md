# TO THE PASCHAL VICTIM
## Easter Sequence
> pre|
Capo 5

[CANTOR]
Am                 A7
To the paschal victim

today let us raise
                   Dm
the sacrifice of praise.

Fmaj7                       E
The lamb has redeemed His flock
Dm               Fmaj7
the innocent has reconciled
                   E
us sinners to the Father.
Am             A7
Death and life faced each other
               Dm
in a wondrous duel.
Fmaj7                   E
The author of life was dead,
     Dm         Fmaj7     E
but now He is alive and triumphs.

A7
Tell us Mary:

what did you see on the way?
Dm                      E
«Empty the tomb of the Lord,
      Dm                         E
the Glory of the Lord and Christ alive,
    F                             E
the angels, the shroud and the sudarium.»

---

[CANTOR]
Dm              E
For Christ, my hope is risen!
F                        E
and He precedes us into Galilee,

[ASSEMBLY]
F                        E
AND HE PRECEDES US INTO GALILEE.

[CANTOR]
Dm           E
Yes, we are sure,
F                E
Christ is truly risen!

[ASSEMBLY]
Dm           E
YES, WE ARE SURE,
   F             E
CHRIST IS TRULY RISEN!
F                        E
AND HE PRECEDES US INTO GALILEE,
F                        E
AND HE PRECEDES US INTO GALILEE.*

[CANTOR]
F                               E
You, immortal King, give us salvation!
