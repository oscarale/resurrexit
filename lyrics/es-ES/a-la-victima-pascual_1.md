# A LA VÍCTIMA PASCUAL
## Secuencia de Pascua
> pre|
Capo 5

[SALMISTA]
Lam             La7
A la víctima pascual

ofrecemos hoy
                     Rem9
el sacrificio de alabanza.

Famaj7                       Mi
El cordero ha redimido el rebaño,
Rem9                    Fa
el inocente ha reconciliado
                  Mi
los pecadores al Padre.

Lam       La7
Muerte y vida se han enfrentado
                  Rem9
en un prodigioso duelo:
Famaj7                    Mi
el Señor de la Vida está muerto,
      Rem9   Fa         Mi
mas ahora está vivo y triunfa.

La7
Dinos tú, María:

¿qué has visto en el camino?

Famaj7                 Mi
«La tumba de Cristo vacía,
Rem9             Fa          Mi
la Gloria del Señor y vivo Cristo,
Fa                              Mi
los ángeles, las vendas y el sudario».

---

[ASAMBLEA]
         Fa
PORQUE CRISTO, MI ESPERANZA,
           Mi
¡HA RESUCITADO!
Fa                    Mi
Y NOS PRECEDE EN GALILEA,
Fa                    Mi
Y NOS PRECEDE EN GALILEA.

Famaj7      Mi
SÍ QUE ES CIERTO,
Famaj7     Fa    Mi
CRISTO HA RESUCITADO.
Famaj7     Mi
SÍ QUE ES CIERTO,
Fa               Mi
CRISTO HA RESUCITADO.
Fa                    Mi
Y NOS PRECEDE EN GALILEA,
Fa                    Mi
Y NOS PRECEDE EN GALILEA.

[SALMISTA]
Fa                                     Mi
Tú, Rey victorioso, danos tú la salvación.
