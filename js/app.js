document.addEventListener('DOMContentLoaded', (event) => {

	const player = new EPlayer();

/* =============================================================================
CONSTANTS
============================================================================= */

	// DOCUMENT
	const doc = document.querySelector('html');
	const body = document.querySelector('body');
	const main = document.querySelector('.main-wrapper');

	// OVERALL PAGE
	const languageLists = document.querySelectorAll('ul.language-list');
	const dropdownBtns = document.querySelectorAll('.dropdown');
	const backBtn = document.querySelector('.back-btn');

	// SELECTOR
	const selector = document.querySelector('.selector');

	// PSALM
	const psalmList = document.querySelector('.psalm-list');
	const psalmListWrapper = document.querySelector('.psalm-list-wrapper');
	const psalmWrapper = document.querySelector('.psalm-wrapper');
	const psalmListMenuWrapper = document.querySelector('.psalm-list-menu-wrapper');
	const psalmLyrics = document.querySelector('.psalm-lyrics');

	// SORT
	const sortBtn = document.querySelector('.sort-btn');
	const sortBtnDown = document.querySelector('.sort-btn-down');
	const sortBtnUp = document.querySelector('.sort-btn-up');

	// TAG MENU
	const tagItems = document.querySelectorAll('.tag-item');
	const tagBtn = document.querySelector('.tag-btn');
	const categoryItems = document.querySelectorAll('.category-item');
	const clearBtn = document.querySelector('.clear-btn');
	const clearBtnCount = document.querySelector('.clear-btn-count');

	// SEARCH
	const searchForm = document.querySelector('.search-form');
	const searchField = document.querySelector('.search-field');

	// PLAYER
	const playerContainer = document.querySelector('.eplayer');
	const playerNull = document.querySelector('.player-null');

	// MODAL
	const modalWrapper = document.querySelector('.modal-wrapper');
	const modalMessage = document.querySelector('.modal-message');
	const modalBtn = document.querySelector('.modal-btn');

/* =============================================================================
GLOBALS
============================================================================= */

	let CURRENT_PSALM = null;
	let DEBUG = true;
	let SEARCHING = false;
	let TRANSITION = 300;

/* =============================================================================
EVENT LISTENERS
============================================================================= */

	sortBtn.addEventListener('click', toggleSort);
	backBtn.addEventListener('click', closePsalm);
	categoryItems.forEach(item => item.addEventListener('click', toggleTag));
	tagItems.forEach(item => item.addEventListener('click', toggleTag));
	tagBtn.addEventListener('click', toggleTags);
	searchForm.addEventListener('submit', search);
	clearBtn.addEventListener('click', clearFilters);
	modalWrapper.addEventListener('click', closeModal);
	modalBtn.addEventListener('click', closeModal);

/* =============================================================================
FUNCTIONS
============================================================================= */

	function search(e) {
		if(DEBUG) console.log(">> FUNCTION: search()");
		e.preventDefault();
		let queryNorm = searchField.value.normalize('NFKD').replace(/[^\w\s.-_'",!?]/g, '').toLowerCase();
		if(queryNorm) {
			SEARCHING = true;
			closePsalm(null);
			fetchPsalms(queryNorm);
			if(DEBUG) console.log("   SCROLLING TO TOP");
			main.scrollIntoView();
		} else {
			SEARCHING = false;
			clearFilters();
		}
	}

	function openModal(modal) {
		modalMessage.textContent = psalms.find(l => { return l.code == doc.lang })[modal];
		modalWrapper.classList.remove('hide');
	}

	function closeModal(e) {
		modalWrapper.classList.add('hide');
	}

	function clearFilters(e) {
		if(DEBUG) console.log(">> FUNCTION: clearFilters()");
		main.scrollIntoView();
		clearBtn.classList.add('hide');
		searchField.value = '';
		categoryItems.forEach(item => item.classList.remove('category-selected'));
		tagItems.forEach(item => item.classList.remove('tag-selected'));
		SEARCHING = false;
		closePsalm(null);
		fetchPsalms();
	}

	function toggleTag(e) {
		if(DEBUG) console.log(">> FUNCTION: toggleTag()");
		if(e.target.classList.contains('category-item')) {
			e.target.classList.toggle('category-selected');
		} else if(e.target.classList.contains('tag-item')) {
			e.target.classList.toggle('tag-selected');
		}

		let categoriesSelected = document.querySelectorAll('.category-selected');
		let tagsSelected = document.querySelectorAll('.tag-selected');
		let query = [];

		if(!categoriesSelected.length && !tagsSelected.length) {
			document.querySelectorAll('.psalm-link').forEach(link => link.classList.remove('hide'));
			if(SEARCHING) countPsalms();
			else clearFilters();
		} else {
			if(categoriesSelected.length && tagsSelected.length) {
				categoriesSelected.forEach(category => {
					tagsSelected.forEach(tag => {
						query.push('.' + category.dataset.value + '.' + tag.dataset.value);
					});
				});
			} else if(categoriesSelected.length) {
				categoriesSelected.forEach(category => query.push('.' + category.dataset.value));
			} else if(tagsSelected.length) {
				tagsSelected.forEach(tag => query.push('.' + tag.dataset.value));
			}

			document.querySelectorAll('.psalm-link').forEach(link => link.classList.add('hide'));
			document.querySelectorAll(query.join(',')).forEach(link => link.classList.remove('hide'));
			countPsalms();
		}
	}

	function toggleTags(e) {
		if(DEBUG) console.log(">> FUNCTION: toggleTags()");
		psalmListMenuWrapper.classList.toggle('open');
	}

	function toggleSort(e) {
		if(DEBUG) console.log(">> FUNCTION: toggleSort()");
		psalmList.classList.toggle('column-reverse');
		if(DEBUG) console.log("   TOGGLING column-reverse", psalmList.classList);
		fetchPsalms();
	}

	function psalmExists(lang, id) {
		psalmsArr = psalms.find(l => { return l.code == lang })['psalms'];
		found = psalmsArr.find(i => { return i.id == id});
		if(found === undefined) return false;
		else return true;
	}

	function setLanguage(e) {
		if(DEBUG) console.log(">> FUNCTION: setLanguage()");
		if(e.target.dataset.lang == doc.lang) {
			if(DEBUG) console.log("   SAME LANGUAGE SELECTED. DO NOT SWITCH");
			return;
		}
		if(CURRENT_PSALM && !psalmExists(e.target.dataset.lang, CURRENT_PSALM.dataset.psalm)) {
			if(DEBUG) console.log("   PSALM DOES NOT EXIST. DO NOT SWITCH");
			openModal('modal_no_psalm');
			return;
		}
		doc.lang = e.target.dataset.lang;
		fetchPsalms();
		if(CURRENT_PSALM) openPsalm(CURRENT_PSALM.dataset.psalm);
		else main.scrollIntoView();
		selector.classList.add('hide');
	}

	function translatePage() {
		if(DEBUG) console.log(">> FUNCTION: translatePage()");
		const translations = document.querySelectorAll('.translate');
		translations.forEach(translation => {
			psalms.forEach(language => {
				if(language['code'] == doc.lang) {
					translation.innerHTML = language[translation.dataset.translate];
				}
			});
		});
	}

	function printLanguageLists(languages) {
		if(DEBUG) console.log(">> FUNCTION: printLanguageLists()");
		languageLists.forEach(languageList => {
			languageLinks = [];
			psalms.forEach(language => {
				languageLink = new LanguageLink(language);
				languageLink.li.addEventListener('click', setLanguage);
				languageLinks.push(languageLink.li);
			});
			languageList.replaceChildren(...languageLinks);
		});
	}

	function printPsalms(data) {
		if(DEBUG) console.log(">> FUNCTION: printPsalms()");
		psalmLinks = [];
		data.forEach(psalm => {
			psalmLink = new PsalmLink(psalm);
			psalmLink.li.addEventListener('click', e => {
				if(e.target.classList.contains('psalm-link') && !e.target.classList.contains('open')) openPsalm(e.target.dataset.psalm);
			});
			psalmLinks.push(psalmLink.li);
		});
		psalmList.replaceChildren(...psalmLinks);
	}

	function openPsalm(psalmId) {
		if(DEBUG) console.log(">> FUNCTION: openPsalm()");
		CURRENT_PSALM = document.querySelector('#p' + psalmId);
		CURRENT_PSALM.appendChild(psalmWrapper);
		CURRENT_PSALM.classList.add('open');
		body.classList.add('open');
		psalmWrapper.classList.remove('hide');
		fetchPsalm(psalmId);
		location.hash = "#" + CURRENT_PSALM.id;
	}

	function closePsalm(e) {
		if(DEBUG) console.log(">> FUNCTION: closePsalm()");
		body.classList.remove('open');
		// player.classList.add('hide');
		psalmWrapper.classList.add('hide');
		history.pushState('', document.title, window.location.pathname + window.location.search);
		if(CURRENT_PSALM) {
			CURRENT_PSALM.classList.remove('open');
			CURRENT_PSALM.scrollIntoView({block: "center"});
		}
		CURRENT_PSALM = null;
		player.stop();
		// playerAudio.pause();
		// playerAudio.currentTime = 0;
	}

	function countPsalms() {
		if(DEBUG) console.log(">> FUNCTION: countPsalms()");
		clearBtn.classList.remove('hide');
		let psalmLinks = document.querySelectorAll('.psalm-link:not(.hide)');
		clearBtnCount.innerHTML = psalmLinks.length;
	}

	function printPsalm(psalm) {
		if(DEBUG) console.log(">> FUNCTION: printPsalm()");
		audioPath = '/audio/' + doc.lang + '/' + psalm['name'] + '_' + psalm['id'] + '.mp3';
		psalmLyrics.innerHTML = psalm['lyrics'];
		loadAudio(audioPath);
	}

	async function loadAudio(path) {
		if(DEBUG) console.log(">> FUNCTION: loadAudio()");
		try {
			const response = await fetch(path);
			if(response.ok) {
				if(DEBUG) console.log("   SETTING SOURCE TO: ", path);
				player.load(path, 'audio/mp3');
				player.play();
				// playerSource.src = path;

				// if(DEBUG) console.log("   SOURCE: ", playerSource);
				playerContainer.classList.remove('hide');
				playerNull.classList.add('hide');

				// playerAudio.load();
				// playerAudio.play();
			} else {
				if(DEBUG) console.log('   AUDIO NOT FOUND, DO NOT SET SOURCE');
				playerContainer.classList.add('hide');
				playerNull.classList.remove('hide');
			}
		} catch(err) {
			console.log('   ERROR WHILE FETCHING AUDIO', err);
		}
	}

	function fetchPsalms(query = '') {
		if(DEBUG) console.log(">> FUNCTION: fetchPsalms()");
		let psalmsData = psalms.find(lang => { return lang.code == doc.lang })['psalms'];
		if(psalmList.classList.contains('column-reverse')) psalmsData = psalmsData.slice().reverse();
		if(query) {
			printPsalms(psalmsData.filter(obj => obj['text'].search(query) >= 0));
			countPsalms();
		} else {
			printPsalms(psalmsData);
		}
		translatePage();
	}

	function fetchPsalm(id) {
		if(DEBUG) console.log(">> FUNCTION: fetchPsalm()");
		psalmsData = psalms.find(lang => { return lang.code == doc.lang });
		printPsalm(psalmsData['psalms'].find(obj => { return obj.id == id }));
		translatePage();
	}

/* =============================================================================
MAIN
============================================================================= */

	printLanguageLists();
	translatePage();
	feather.replace();
});
