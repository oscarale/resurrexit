class PsalmLink {
	constructor(obj) {
		this.li = document.createElement('li');
		this.li.id = "p" + obj.id;
		this.li.classList.add('psalm-link');
		this.li.classList.add(obj.category);
		this.li.dataset.psalm = obj.id.toString();
		if(obj.tags) obj.tags.split(',').forEach(tag => this.li.classList.add(tag));

		this.title = document.createElement('h4');
		this.title.classList.add('psalm-title');
		this.title.classList.add('text-center');
		this.title.innerHTML = obj.title;

		this.subtitle = document.createElement('h5');
		this.subtitle.classList.add('psalm-subtitle');
		this.subtitle.classList.add('text-center');
		this.subtitle.innerHTML = obj.subtitle;

		this.li.appendChild(this.title);
		this.li.appendChild(this.subtitle);

		if(obj.tags) {
			this.tags = document.createElement('div');
			this.tags.classList.add('tags');
			obj.tags.split(',').forEach(tag => {
				console.log(obj.id + ' tag: ' + tag);
				let tagSpan = document.createElement('span');
				tagSpan.classList.add(tag);
				tagSpan.classList.add('badge');
				tagSpan.classList.add('badge-tag');
				tagSpan.classList.add('translate');
				tagSpan.dataset.translate = 'tag_' + tag;
				tagSpan.innerHTML = tag;

				this.tags.appendChild(tagSpan);
			});
			this.li.appendChild(this.tags);
		}
	}
}

class LanguageLink {
	constructor(obj) {
		this.li = document.createElement('li');
		this.li.dataset.lang = obj.code;
		this.li.innerHTML = obj.name;
	}
}
