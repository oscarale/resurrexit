
<a name="v0.1.0"></a>
## v0.1.0

> 2023-09-13

### Bug Fixes

* **web:** Can switch to same psalm in different language. Closes [#22](https://git.aleyoscar.com/emet/resurrexit/issues/22).
* **web:** Psalm list opens correctly when sorted Z>A. Closes [#44](https://git.aleyoscar.com/emet/resurrexit/issues/44).

### Features

* **core:** Initial release and start tracking changes.

