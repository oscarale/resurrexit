import json

path = "../lyrics/"

languages = [
	"es-ES",
	"en-US"
]

for lang in languages:
	jsonFilePath = '../json/psalms_' + lang + '.json'
	with open(jsonFilePath, 'r', encoding='utf-8') as d:
		j_data = json.load(d)
	for psalm in j_data:
		psalm_file = path + lang + "/" + psalm['name'] + "_" + str(psalm['id']) + ".md"
		print(">> WRITING TO FILE: " + psalm_file)
		with open(psalm_file, 'w', encoding='utf-8') as f:
			f.write("# " + psalm['title'] + "\n")
			f.write("## " + psalm['subtitle'] + "\n")
			f.write("> " + psalm['category'] + "|" + psalm['tags'] + "\n")
			f.write("Capo 0\n")
