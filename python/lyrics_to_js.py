import json
from os import listdir, rename
from os.path import isfile, join
from unidecode import unidecode
import re

lyrics_base = "../lyrics/"
audio_base = "../audio/"
js_file = "../js/psalms.js"

debug_json = False

count = 0

def simplify(text):
	new_text = unidecode(text).lower()
	new_text = re.sub(r'[^a-z0-9 ]+', '', new_text)
	return ' '.join(new_text.split())

def extract_text(text):
	new_text = unidecode(text).lower()
	return new_text

def insert_chords(c, text):
	c = c.rstrip()
	while c:
		i = c.rfind(' ') + 1
		text = text[:i] + '<span class="chord">' + c[i:] + '</span>' + text[i:]
		c = c[:i].rstrip()
	return text

psalms = []

languages_list = [f for f in listdir(lyrics_base) if isfile(join(lyrics_base, f))]
for language_file in languages_list:
	with open(lyrics_base + language_file, 'r', encoding='utf-8') as j:
		language_data = json.load(j)

	lyrics_path = lyrics_base + language_data['code'] + '/'
	lyrics_list = [f for f in listdir(lyrics_path) if isfile(join(lyrics_path, f))]

	language_count = 0
	language_data['psalms'] = []

	# PARSE EACH FILE IN THE LYRICS DIRECTORY
	for lyric in lyrics_list:
		language_count += 1
		count += 1
		psalm = {}

		# CREATE PSALM JSON STRUCTURE
		psalm['id'] = 0
		psalm['name'] = ''
		psalm['title'] = ''
		psalm['subtitle'] = ''
		psalm['category'] = ''
		psalm['tags'] = ''
		psalm['capo'] = 0
		psalm['text'] = ''
		psalm['lyrics'] = '<div class="lyrics-col">'

		# VARIABLE TO STORE ALL SEARCHABLE TEXT
		raw_text = ''

		# PARSE EACH LINE OF LYRIC FILE
		with open(lyrics_path + lyric, encoding='utf-8') as f:
			psalm['id'] = lyric[(lyric.rfind('_') + 1):-3] # PSALM ID IS AFTER THE _
			# psalm['name'] = lyric[:lyric.rfind('_')] # PSALM NAME IS BEFORE THE _
			current_section = ''
			chords = ''
			for line in f:
				stripped = line.strip()
				if stripped.startswith('# '): # PSALM TITLE IS MD H1
					psalm['title'] = stripped[2:]
					psalm['name'] = simplify(psalm['title']).replace(' ', '-')
					raw_text += psalm['title'] + ' '
				elif stripped.startswith('## '): # PSALM SUBTITLE IS MD H2
					psalm['subtitle'] = stripped[3:]
					raw_text += psalm['subtitle'] + ' '
				elif stripped.startswith('> '): # PSALM CATEGORIES/TAGS IS AN MD QUOTE
					psalm['category'] = stripped[2:].split('|')[0] # FIRST TAG BEFORE | IS THE CATEGORY
					psalm['tags'] = stripped[2:].split('|')[1] # SECOND SET OF TAGS AFTER | ARE THE TAGS
				elif stripped.lower().startswith('capo '):
					psalm['capo'] = stripped[5:] # CAPO JUST STARTS WITH THE WORD Capo
				elif stripped.startswith('['): # START PARSING PSALM SECTION
					if current_section:
						psalm['lyrics'] += '</div>'
					section = stripped.strip('[]')
					current_section = 'sec_' + section
					psalm['lyrics'] += '<div class="' + language_data[current_section] + '">'
				elif stripped.startswith('---') and current_section: # LYRICS PAGE BREAK
					psalm['lyrics'] += '</div></div><div class="lyrics-col">' # END PREVIOUS SECTION AND COLUMN AND START ANOTHER
					current_section = ''
				elif chords:
					psalm['lyrics'] += '<p class="lyric">' + insert_chords(chords, stripped) + '</p>'
					raw_text += stripped + ' '
					chords = ''
				elif stripped and set(line.split()).issubset(language_data['chords']):
					chords = line.rstrip()
				elif stripped:
					psalm['lyrics'] += '<p class="lyric">' + stripped + '</p>'
					raw_text += stripped + ' '
				elif current_section:
					psalm['lyrics'] += '<div class="spacer"></div>'

		# CLOSE LYRICS DIV
		psalm['lyrics'] += '</div>'

		# PARSE RAW TEXT AND REMOVE DUPLICATE WORDS
		psalm['text'] = extract_text(raw_text)

		# ADD PSALM TO PSALMS ARRAY
		language_data['psalms'].append(psalm)

		# RENAME LYRIC FILE TO MATCH LYRIC NAME IF IT HAS BEEN UPDATED
		lyric_name = lyric[:lyric.rfind('_')]
		if lyric_name != psalm['name']:
			rename(lyrics_path + lyric, lyrics_path + psalm['name'] + '_' + psalm['id'] + '.md')
			print(">> RENAMED LYRIC FILE " + lyric_name + " to " + psalm['name'])

		# RENAME AUDIO FILE IF IT HAS BEEN UPDATED
		audio_path = audio_base + language_data['code'] + '/'
		audio_list = [f for f in listdir(audio_path) if isfile(join(audio_path, f))]
		for audio in audio_list:
			audio_id = audio[(audio.rfind('_') + 1):-4] # AUDIO ID IS AFTER THE _
			audio_name = audio[:audio.rfind('_')] # AUDIO NAME IS BEFORE THE _
			if audio_id == psalm['id']:
				if audio_name != psalm['name']:
					rename(audio_path + audio, audio_path + psalm['name'] + '_' + psalm['id'] + '.mp3')
					print(">> RENAMED AUDIO FILE " + audio_name + " to " + psalm['name'])

	# SORT PSALMS ARRAY BY TITLE
	language_data['psalms'] = sorted(language_data['psalms'], key=lambda x: x['title'].lower())

	psalms.append(language_data)

	print()
	print(">> FINISHED PROCESSING (" + str(language_count) + ") LYRIC FILES FOR " + language_data['code'])

# WRITE JSON FILE
with open(js_file, 'w', encoding='utf-8') as f:
	if debug_json:
		json.dump(psalms, f, indent=4, ensure_ascii=False)
	else:
		json.dump(psalms, f, ensure_ascii=False, separators=(',', ':'))

if not debug_json:
	with open(js_file, 'r+', encoding='utf-8') as f:
		original = f.read()
		f.seek(0)
		f.write("psalms=" + original + ';')

print()
print(">> FINISHED PROCESSING (" + str(count) + ") LYRIC FILES FOR ALL LANGUAGES")
