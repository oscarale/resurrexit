import os
import eyed3
import json

audio_path = "../audio/es-ES/"
json_path = "../json/psalms_es-ES.json"
album = "Resucitó"
artist = "Kiko Argüello"

for mp3 in [f for f in os.listdir(audio_path) if os.path.isfile(os.path.join(audio_path, f))]:
	audio = eyed3.load(audio_path + mp3)
	audio_id = int(mp3[(mp3.rfind('_') + 1):-4])
	with open(json_path, 'r', encoding='utf-8') as j:
		j_data = json.load(j)
	for psalm in j_data:
		if int(psalm['id']) == audio_id:
			print("EDITING PSALM WITH ID " + str(audio_id))
			audio.tag.artist = artist
			audio.tag.album = album
			audio.tag.album_artist = artist
			audio.tag.title = psalm['title']
			audio.tag.track_num = psalm['id']

			audio.tag.save(encoding='utf-8')
			break;
