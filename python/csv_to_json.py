import csv
import json

def csv_to_json(csvFilePath, jsonFilePath):
	jsonArray = []

	with open(csvFilePath, encoding='utf-8')as csvf:
		csvReader = csv.DictReader(csvf)

		for row in csvReader:
			jsonArray.append(row)

	with open(jsonFilePath, 'w', encoding='utf-8') as jsonf:
		jsonString = json.dumps(jsonArray, indent=4, ensure_ascii=False)
		jsonf.write(jsonString)

languages = [
	'es-ES',
	'en-US'
]

for lang in languages:
	csvFilePath = r'../csv/psalms_' + lang + r'.csv'
	jsonFilePath = r'../json/psalms_' + lang + r'.json'

	csv_to_json(csvFilePath, jsonFilePath)
	print("CONVERTED CSV:" + csvFilePath + " to JSON: " + jsonFilePath)
