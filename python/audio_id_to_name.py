import os
import json

audio_path = "../audio/es-ES/"
json_path = "../json/psalms_es-ES.json"

for count, f in enumerate(os.listdir(audio_path)):
	o_name, o_ext = os.path.splitext(f)
	n_name = ""
	o_name = o_name.strip()
	with open(json_path, 'r', encoding='utf-8') as j:
		j_data = json.load(j)
	for psalm in j_data:
		# print("CHECKING PSALM # " + psalm['id'] + " WITH ORIGINAL: " + o_name)
		if int(psalm['id']) == int(o_name):
			n_name = psalm['name'] + '_' + o_name + o_ext
			# print("FOUND MATCH!: " + psalm['id'] + " = " + o_name)
			break;
	if n_name != '':
		os.rename(audio_path + f, audio_path + n_name)
		print("RENAMED FILE: " + f + " TO: " + n_name)
	else:
		print(">> FAILED TO FIND PSALM WITH ID: " + o_name)
