# [Resurrexit](https://resurrexit.app)

> v0.0.1

A multi-lingual songbook and tool set for the Neocatechumenal Way. Written in vanilla HTML, CSS and Javascript as a PWA.

### Contents

- [Features](#features)
- [Languages](#languages)
- [Latin](#latin-abbreviations)

### Features

- [x] Full list of all psalms
- [x] Audio for majority of psalms
- [ ] Chords and Lyrics
- [x] Search
- [ ] Transposition to your personal key
- [ ] Chord diagrams
- [ ] Personal psalm lists for word & eucharist celebrations
- [ ] Personal tags

### Languages

- [x] English - United States
- [x] Spanish - Spain

### Latin Abbreviations

| Name				| Abb 	|
| :--				| :---:	|
| Precatechumenate	| pre	|
| Catechumenate		| cat	|
| Election			| ele	|
| Liturgical		| lit	|
| ---				| ---	|
| Bread				| pan	|
| Children			| lib	|
| Communion			| vin	|
| Easter			| pas	|
| Entrance			| int	|
| Final				| fin	|
| Laudes			| lau	|
| Lent				| com	|
| Peace				| pax	|
| Pentecost			| pen	|
| Virgin			| vir	|
