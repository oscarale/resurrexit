
<a name="v0.1.0"></a>
## v0.1.0

> 2023-09-13

### Doc

* **core:** Updated README.md with instructions

### Fix

* **web:** Updated cpanel.yml for deployment.

